import puppeteer from "puppeteer";
import { parse } from "date-fns";

export default async function getLatestCourses() {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto("https://www.pluralsight.com/search?categories=course&sort=displayDate", {
    waitUntil: "networkidle0",
  });

  const coursesSelector = "#search-results .search-result";
  const dateSelector = ".search-result__date";
  const titleSelector = ".search-result__title";
  const levelSelector = ".search-result__level";
  const durationSelector = ".search-result__length";

  const latestCourses = await page.evaluate(
    selectors => {
      const courses = Array.from(document.querySelectorAll(selectors.course));
      return courses.map(course => ({
        date: course.querySelector(selectors.date).textContent,
        title: course.querySelector(selectors.title).textContent,
        level: course.querySelector(selectors.level).textContent,
        duration: course.querySelector(selectors.duration).textContent,
        link: course.querySelector(`${selectors.title} a`).href,
      }));
    },
    {
      level: levelSelector,
      duration: durationSelector,
      course: coursesSelector,
      date: dateSelector,
      title: titleSelector,
    }
  );
  await browser.close();

  return latestCourses.map(c => ({ ...c, date: parse(c.date) }));
}
