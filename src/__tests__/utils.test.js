import * as utils from "../utils";

describe("Utils", () => {
  describe("byDate", () => {
    const january = new Date(2017, 0, 1);
    const february = new Date(2017, 1, 1);
    const march = new Date(2017, 2, 1);
    it("is a function that accepts two arguments", () => {
      expect(utils.byDate).toEqual(expect.any(Function));
      expect(utils.byDate).toHaveLength(2);
    });

    it("returns 0 when items have the same date", () => {
      const result = utils.byDate({ date: january }, { date: january });
      expect(result).toEqual(0);
    });

    it("returns negative when first item should be first in sort order", () => {
      const result = utils.byDate({ date: january }, { date: february });
      expect(result).toBeLessThan(0);
    });

    it("returns positive when first item should be second in sort order", () => {
      const result = utils.byDate({ date: march }, { date: february });
      expect(result).toBeGreaterThan(0);
    });

    it("works as a sort function when passed to Array#sort", () => {
      const expectedSortedArray = [january, february, march].map(date => ({ date }));
      const result = [february, january, march].map(date => ({ date })).sort(utils.byDate);

      expect(result).toEqual(expectedSortedArray);
    });
  });
});
