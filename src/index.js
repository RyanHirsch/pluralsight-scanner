import R from "ramda";
import { format } from "date-fns";

import getLatestCourses from "./get-latest";
import { byDate } from "./utils";

(async () => {
  const courses = await getLatestCourses();
  const sortedCourses = R.sort(byDate, courses);
  sortedCourses
    .slice(-6)
    .map(x => `${format(x.date, "ddd MM/DD")}   ${x.title}  [${x.level}]`)
    .forEach(s => console.log(s));
})();
